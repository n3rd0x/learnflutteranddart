/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../pages/module050_product_edit.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductListPage extends StatelessWidget {
  final Function updateProduct;
  final List<Map<String, dynamic>> mProducts;

  ProductListPage(this.mProducts, this.updateProduct);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          leading: ConstrainedBox(
            constraints: BoxConstraints(
                minWidth: 40, minHeight: 20, maxWidth: 60, maxHeight: 40),
            child: Image.asset(mProducts[index]['image']),
          ),
          title: Text(mProducts[index]['title']),
          trailing: IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return ProductEditPage(
                        product: mProducts[index],
                        updateProduct: updateProduct,
                        productIndex: index);
                  },
                ),
              );
            },
          ),
        );
      },
      itemCount: mProducts.length,
    );
  }
}
