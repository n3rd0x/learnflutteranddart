/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ProductEditPage extends StatefulWidget {
  final Function addProduct;
  final Function updateProduct;
  final Map<String, dynamic> product;
  final int productIndex;

  ProductEditPage(
      {this.addProduct, this.updateProduct, this.product, this.productIndex});

  @override
  State<StatefulWidget> createState() {
    return _ProductEditPageState();
  }
}

class _ProductEditPageState extends State<ProductEditPage> {
  final Map<String, dynamic> mFormData = {
    'title': null,
    'description': null,
    'price': null,
    'image': 'assets/food.jpeg'
  };
  final GlobalKey<FormState> mFormKey = GlobalKey<FormState>();

  Widget _buildTitleTextField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Title"),
        initialValue: widget.product == null ? '' : widget.product['title'],
        validator: (String value) {
          if (value.isEmpty) {
            return 'Title is required!';
          }
        },
        onSaved: (String value) {
          mFormData['title'] = value;
        });
  }

  Widget _buildDescriptionTextField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Description"),
        // We intentionally skip the validation here, because we
        // want to make the description optional.
        initialValue:
            widget.product == null ? '' : widget.product['description'],
        onSaved: (String value) {
          mFormData['description'] = value;
        });
  }

  Widget _buildPriceTextField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Price"),
        initialValue:
            widget.product == null ? '' : widget.product['price'].toString(),
        validator: (String value) {
          if (value.isEmpty ||
              !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value)) {
            return 'Price is required and should be a number!';
          }
        },
        keyboardType: TextInputType.number,
        onSaved: (String value) {
          mFormData['price'] = double.parse(value);
        });
  }

  void _submitForm() {
    if (!mFormKey.currentState.validate()) {
      return;
    }
    mFormKey.currentState.save();
    if (widget.product == null) {
      widget.addProduct(mFormData);
    } else {
      widget.updateProduct(widget.productIndex, mFormData);
    }
    Navigator.pushReplacementNamed(context, '/products');
  }

  Widget _buildPageContent(double targetPadding) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: Form(
          key: mFormKey,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: targetPadding / 2),
            children: <Widget>[
              _buildTitleTextField(),
              _buildDescriptionTextField(),
              _buildPriceTextField(),
              SizedBox(height: 10.0),
              RaisedButton(
                child: Text('Save'),
                textColor: Colors.white,
                onPressed: _submitForm,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPageWithContent(double targetPadding) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Page'),
      ),
      body: _buildPageContent(targetPadding),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductEditPage] Starting.');
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    final double targetPadding = deviceWidth - targetWidth;
    return widget.product == null
        ? _buildPageContent(targetPadding)
        : _buildPageWithContent(targetPadding);
  }
}
