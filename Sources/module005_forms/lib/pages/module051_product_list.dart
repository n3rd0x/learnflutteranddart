/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../pages/module051_product_edit.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductListPage extends StatelessWidget {
  final Function updateProduct;
  final Function deleteProduct;
  final List<Map<String, dynamic>> mProducts;

  ProductListPage(this.mProducts, this.updateProduct, this.deleteProduct);

  @override
  Widget build(BuildContext context) {
    print('[ProductListPage] Starting.');
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return Dismissible(
          key: Key(mProducts[index]['title']),
          onDismissed: (DismissDirection direction) {
            if (direction == DismissDirection.endToStart) {
              deleteProduct(index);
            }
          },
          background: Container(color: Colors.red),
          child: Column(
            children: <Widget>[
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage(mProducts[index]['image']),
                ),
                title: Text(mProducts[index]['title']),
                subtitle: Text('\$${mProducts[index]['price'].toString()}'),
                trailing: IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) {
                          return ProductEditPage(
                              product: mProducts[index],
                              updateProduct: updateProduct,
                              productIndex: index);
                        },
                      ),
                    );
                  },
                ),
              ),
              Divider(),
            ],
          ),
        );
      },
      itemCount: mProducts.length,
    );
  }
}
