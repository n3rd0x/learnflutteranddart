/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductCreatePage extends StatefulWidget {
  final Function addProduct;

  ProductCreatePage(this.addProduct);

  @override
  State<StatefulWidget> createState() {
    return _ProductCreatePageState();
  }
}

class _ProductCreatePageState extends State<ProductCreatePage> {
  String mTitle = '';
  String mDescription = "";
  double mPrice = 0.0;
  final GlobalKey<FormState> mFormKey = GlobalKey<FormState>();

  Widget _buildTitleTextField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Title"),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Title is required!';
          }
        },
        onSaved: (String value) {
          setState(() {
            mTitle = value;
          });
        });
  }

  Widget _buildDescriptionTextField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Description"),
        // We intentionally skip the validation here, because we
        // want to make the description optional.
        onSaved: (String value) {
          setState(() {
            mDescription = value;
          });
        });
  }

  Widget _buildPriceTextField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Price"),
        validator: (String value) {
          if (value.isEmpty ||
              !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value)) {
            return 'Price is required and should be a number!';
          }
        },
        keyboardType: TextInputType.number,
        onSaved: (String value) {
          setState(() {
            mPrice = double.parse(value);
          });
        });
  }

  void _submitForm() {
    if (!mFormKey.currentState.validate()) {
      return;
    }
    mFormKey.currentState.save();
    final Map<String, dynamic> product = {
      'title': mTitle,
      'description': mDescription,
      'price': mPrice,
      'image': 'assets/food.jpeg'
    };
    widget.addProduct(product);
    Navigator.pushReplacementNamed(context, '/products');
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    final double targetPadding = deviceWidth - targetWidth;
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Form(
        key: mFormKey,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: targetPadding / 2),
          children: <Widget>[
            _buildTitleTextField(),
            _buildDescriptionTextField(),
            _buildPriceTextField(),
            SizedBox(height: 10.0),
            RaisedButton(
              child: Text('Save'),
              textColor: Colors.white,
              onPressed: _submitForm,
            )
          ],
        ),
      ),
    );
  }
}
