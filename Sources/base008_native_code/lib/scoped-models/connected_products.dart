/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'dart:async';
import 'dart:convert';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rxdart/subjects.dart';
import '../models/auth.dart';
import '../models/product.dart';
import '../models/user.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ConnectedProductsModel extends Model {
  final String _imageUrl =
      'https://cdn-prod.medicalnewstoday.com/content/images/articles/317/317122/junk-food.jpg';
  List<Product> _products = [];
  String _selectedId;
  String _apiKey = '';
  String _database = '';
  User _authUser = null;
  bool _isLoading = false;
  bool _isRefreshing = false;

  void setServer(String db, String key) {
    _database = db + '/products';
    _apiKey = key;
  }
}

class ProductModel extends ConnectedProductsModel {
  bool _showFavorites = false;

  Future<String> addProduct(
      String title, String description, String image, double price) async {
    _isLoading = true;
    notifyListeners();

    // Create a post to the Firebase.
    final Map<String, dynamic> productData = {
      'title': title,
      'description': description,
      'image': _imageUrl,
      'price': price,
      'userEmail': _authUser.email,
      'userId': _authUser.id
    };

    print('[ConnectedProduct] Attempt adding product(' + title + ').');

    try {
      // This would automaticcally convert into the same code as
      // previous when using "then".
      final http.Response response = await http.post(
          _database + '.json?auth=${_authUser.token}',
          body: json.encode(productData));
      final Map<String, dynamic> responseData = json.decode(response.body);
      print('-- Response --');
      print(responseData);
      print('--------------');

      // Check error status.
      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;

        print(
            '[ConnectedProduct] StatusCode: ' + response.statusCode.toString());
        return 'Status code: ' + response.statusCode.toString();
      }

      print('[ConnectedProduct] addProduct: ' + responseData.toString());
      final Product newProduct = Product(
          id: responseData['name'],
          title: title,
          description: description,
          image: image,
          price: price,
          userEmail: _authUser.email,
          userId: _authUser.id);

      _products.add(newProduct);

      _isLoading = false;
      notifyListeners();

      print('[ConnectedProduct] Successful adding the product.');
      return null;
    } catch (error) {
      _isLoading = false;
      notifyListeners();

      print('[ConnectedProduct] Exception: ' + error.toString());
      return error.toString();
    }
  }

  List<Product> get allProducts {
    return List.from(_products);
  }

  List<Product> get displayProducts {
    if (_showFavorites) {
      // Alternative:
      // List.from(_products.where((Product product) => product.isFavorite).toList());
      return List.from(_products.where((Product product) {
        return product.isFavorite;
      }).toList());
    }
    return List.from(_products);
  }

  Future<Null> deleteProduct() async {
    print('[ConnectedProduct] Delete product(' + selectedProduct.id);
    final deletedId = selectedProduct.id;
    _isLoading = true;
    _products.removeAt(selectedIndex);
    _selectedId = null;
    notifyListeners();

    final responseData = await http
        .delete(_database + '/${deletedId}.json?auth=${_authUser.token}');
    print('[ConnectedProduct] Delete Product Response');
    print('-- Response --');
    print(responseData);
    print('--------------');

    _isLoading = false;
    notifyListeners();
    return null;
  }

  Future<Null> fetchProducts({onlyForUser = false}) {
    _isLoading = true;
    notifyListeners();

    return http
        .get(_database + '.json?auth=${_authUser.token}')
        .then<Null>((http.Response response) {
      print('[fetchProducts] fetchProducts Response');

      // Data structure: Map<String, Map<String, dynamic>>
      Map<String, dynamic> responseData = json.decode(response.body);
      if (responseData == null || responseData.containsKey('error')) {
        _isLoading = false;
        notifyListeners();
        return;
      }

      final List<Product> products = [];

      responseData.forEach((String id, dynamic itr) {
        print('[fetchProducts] Process ID: ' + id);
        var isFavorite = false;
        var wishlist = itr['wishlist'];
        if ((wishlist != null) && wishlist.containsKey(_authUser.id)) {
          isFavorite = true;
        }
        products.add(
          Product(
            id: id,
            title: itr['title'],
            description: itr['description'],
            image: itr['image'],
            price: itr['price'], // The data is already a double.
            userEmail: itr['userEmail'],
            userId: itr['userId'],
            isFavorite: isFavorite,
          ),
        );
      });

      // Filter on user ID if specified. Otherwise retrieve all products.
      _products = onlyForUser
          ? products.where((Product itr) {
              return itr.userId == _authUser.id;
            }).toList()
          : products;
      _isLoading = false;
      notifyListeners();
    });
  }

  bool get isShowFavorites {
    return _showFavorites;
  }

  Future<Null> refreshProducts() {
    _isRefreshing = true;
    return fetchProducts().then((_) {
      _isRefreshing = false;
    });
  }

  Product product(String id) {
    return allProducts.firstWhere((Product product) {
      return product.id == id;
    });
  }

  String get selectedId {
    return _selectedId;
  }

  int get selectedIndex {
    return _products.indexWhere((Product product) {
      return product.id == _selectedId;
    });
  }

  Product get selectedProduct {
    if (_selectedId != null) {
      return _products.firstWhere((Product product) {
        return product.id == _selectedId;
      });
    }
    return null;
  }

  void selectProduct(String id) {
    _selectedId = id;
  }

  void toggleDisplayFavorite() {
    _showFavorites = !_showFavorites;
    notifyListeners();
  }

  void toggleFavorite() async {
    final bool curState = _products[selectedIndex].isFavorite;
    final bool newState = !curState;
    final url = _database +
        '/${selectedProduct.id}/wishlist/${_authUser.id}.json?auth=${_authUser.token}';
    bool success = true;
    if (newState) {
      final response = await http.put(url, body: json.encode(true));
      if (response.statusCode != 200 && response.statusCode != 201) {
        print(
            '[toggleFavorite] Failed to update the wish list of user (${_authUser.id}.');
        success = false;
      }
    } else {
      final response = await http.delete(url);
      if (response.statusCode != 200 && response.statusCode != 201) {
        print(
            '[toggleFavorite] Failed to delete the user (${_authUser.id} off the wish list.');
        success = false;
      }
    }

    if (success) {
      final Product updateProduct = Product(
          id: selectedProduct.id,
          title: selectedProduct.title,
          description: selectedProduct.description,
          price: selectedProduct.price,
          image: selectedProduct.image,
          userEmail: selectedProduct.userEmail,
          userId: selectedProduct.userId,
          isFavorite: newState);
      _products[selectedIndex] = updateProduct;
    }

    // Notify that the data have changed so the widget would be rebuild
    // with the new data. This is useful for widget that we want live updates.
    // Otherwise the update would be applied when we revisit the widget.
    notifyListeners();
  }

  Future<bool> updateProduct(
      String title, String description, String image, double price) {
    _isLoading = true;
    notifyListeners();

    // Update data to database.
    final Map<String, dynamic> data = {
      'title': title,
      'description': description,
      'image': _imageUrl,
      'price': price,
      'userEmail': selectedProduct.userEmail,
      'userId': selectedProduct.userId
    };
    return http
        .put(_database + "/${selectedProduct.id}.json?auth=${_authUser.token}",
            body: json.encode(data))
        .then<bool>((http.Response response) {
      // Check error status.
      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;
        notifyListeners();
        return false;
      }

      _products[selectedIndex] = Product(
          id: selectedProduct.id,
          title: title,
          description: description,
          image: image,
          price: price,
          userEmail: selectedProduct.userEmail,
          userId: selectedProduct.userId);

      _isLoading = false;
      notifyListeners();
      return true;
    }).catchError((onError) {
      _isLoading = false;
      notifyListeners();
      return false;
    });
  }
}

class UserModel extends ConnectedProductsModel {
  Timer _authTimer = null;
  PublishSubject<bool> _userSubject = PublishSubject();

  void autoAuthenticate() async {
    print('[UserModel] Auto authenticate.');
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('UserToken');
    final id = prefs.getString('UserId');
    final email = prefs.getString('UserEmail');
    final password = prefs.getString('UserPassword');
    final database = prefs.getString('Database');
    final apiKey = prefs.getString('ApiKey');
    final expiredTime = prefs.getString('ExpiredTime');

    // Check for valid values.
    bool valid = true;
    if (token == null) {
      valid = false;
    }
    if (id == null) {
      valid = false;
    }
    if (email == null) {
      valid = false;
    }
    if (password == null) {
      valid = false;
    }
    if (database == null) {
      valid = false;
    }
    if (apiKey == null) {
      valid = false;
    }
    if (expiredTime == null) {
      valid = false;
    }

    // Auto authenticate the user.
    if (valid) {
      print('[UserModel] Authenticate returning user process.');

      final now = DateTime.now();
      final endTime = DateTime.parse(expiredTime);
      if (!endTime.isBefore(now)) {
        print('[UserModel] User auto authenticated.');
        _authUser = User(
          id: id,
          email: email,
          password: password,
          token: token,
        );

        final tokenLifespan = endTime.difference(now).inSeconds;
        setAuthTimeout(tokenLifespan);
        setServer(database, apiKey);

        _userSubject.add(true);
        _authUser.printUserInfo();
        print('[UserModel] Token will expired in ' + tokenLifespan.toString());
      } else {
        print('[UserModel] Token expired!');
        _authUser = null;
      }

      notifyListeners();
    }
  }

  Future<Map<String, dynamic>> authenticate(String email, String password,
      [AuthMode mode = AuthMode.Login]) async {
    _isLoading = true;
    notifyListeners();

    var postMode = 'signInWithPassword';
    if (mode == AuthMode.SignUp) {
      postMode = 'signUp';
    }

    // Map<String, dynamic>
    final data = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };
    final response = await http.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:${postMode}?key=${_apiKey}',
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );

    final responseData = json.decode(response.body);
    bool result = false;
    var message = 'Process has uhandled error.';
    if (responseData.containsKey('idToken')) {
      result = true;
      message = 'Authenication succeeded!';
      _authUser = User(
          id: responseData['localId'],
          email: email,
          password: password,
          token: responseData['idToken']);
      int expiredSeconds = int.parse(responseData['expiresIn']);
      setAuthTimeout(expiredSeconds);

      final now = DateTime.now();
      final expiredTime = now.add(Duration(seconds: expiredSeconds));
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('UserToken', _authUser.token);
      prefs.setString('UserId', _authUser.id);
      prefs.setString('UserEmail', _authUser.email);
      prefs.setString('UserPassword', _authUser.password);
      prefs.setString('ExpiredTime', expiredTime.toIso8601String());

      _userSubject.add(true);
    } else if (responseData['error']['message'] == 'EMAIL_NOT_FOUND') {
      message = 'This email doesn\'t exists!';
    } else if (responseData['error']['message'] == 'INVALID_PASSWORD') {
      message = 'The password is invalid!';
    } else if (responseData['error']['message'] == 'EMAIL_EXISTS') {
      message = 'This email already exists!';
    }

    print(
        'Response of authenticate (${mode == AuthMode.Login ? 'login' : 'sign up'}):');
    print('-- Response --');
    print(responseData);
    print('--------------');

    _isLoading = false;
    notifyListeners();
    return {'success': result, 'message': message};
  }

  void logout() async {
    _authUser = null;
    _authTimer.cancel();

    final prefs = await SharedPreferences.getInstance();
    prefs.remove('UserToken');
    prefs.remove('UserId');

    // For testing purpose, we still want to
    // save these, so we don't need to enter
    // the credentials again.
    //prefs.remove('UserEmail');
    //prefs.remove('UserPassword');

    _userSubject.add(false);
  }

  void printUserInfo() {
    if (_authUser != null) {
      _authUser.printUserInfo();
    } else {
      print('WARNING! NO CURRENT USER ACTIVATED!');
    }
  }

  void setAuthTimeout(int time) {
    print('[UserModel] Auto logout in ' + time.toString() + ' seconds');
    _authTimer = Timer(Duration(seconds: time), logout);
  }

  User get user {
    return _authUser;
  }

  PublishSubject<bool> get userSubject {
    return _userSubject;
  }
}

class UtilityModel extends ConnectedProductsModel {
  bool get isLoading {
    return _isLoading;
  }

  bool get isRefreshing {
    return _isRefreshing;
  }
}
