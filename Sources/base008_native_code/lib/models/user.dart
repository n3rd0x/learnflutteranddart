/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class User {
  final String id;
  final String email;
  final String password;
  final String token;

  User(
      {@required this.id,
      @required this.email,
      @required this.password,
      @required this.token});

  bool isValid() {
    return id != null;
  }

  void printUserInfo() {
    print('-- USER INFO --');
    print('ID:       ' + id);
    print('Email:    ' + email);
    print('Password: ' + password);
    print('---------------');
  }
}
