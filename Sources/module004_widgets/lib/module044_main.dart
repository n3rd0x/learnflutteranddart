/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Tips
// ************************************************************
// ----------------------------------------
// Generic
// ----------------------------------------
// * Underscore
//      Use when we want thing to be private.
// * Final
//      Make the target imutable, so we can't change the reference of
//      the variable. Or assign new reference or value.
//      NB! For list we can still modify its value, but not assign
//      new list.
// * Const
//      This make we can't change the value.
//
// ----------------------------------------
// Setup Assets
// ----------------------------------------
// In order to use the assets directory, we have to edit the pubspec.yaml.
// Uncomment the "asset" section and add files to use.
// This would be bundle in the app during shipment.
//
// ----------------------------------------
// App Name
// ----------------------------------------
// Android: Edit AndroidManifest.xml
// IOS:     Edit Info.plist
//
// ----------------------------------------
// Widget Types
// ----------------------------------------
// StatelessWidget
//  * Only take data as input (optional), then render a new widget tree.
//  * Useful for render static page.
// StatefulWidget
//  * Work with external and internal data which can be changed.
//  * Useful for dynamic page.
//  * Offer additional lifecycle methods.
// Changes to external or internal data lead to a rerender cycle.
//
// ----------------------------------------
// Rebuild Widgets
// ----------------------------------------
// State widgets can be rebuild when we are passing new
// values into the contructor, which will again call the build
// function.
//
// ----------------------------------------
// Keywords
// ----------------------------------------
// * widget (properties)
//    Allow access between the connected state classes.
//
// ----------------------------------------
// Arguments
// ----------------------------------------
// * Positional Arguments
//    Arguments are passed by its position declarations.
//    Example:
//        ProductManager(this.ProductName, this.NumOfProducts)
//        Usage:
//        ProductManager('Banh Xeo', 14)
// * Named Arguments
//    Arguments are passed by name.
//    Example:
//        ProductManager({this.ProductName}, {this.NumOfProducts})
//        Usage:
//        ProductManager(ProductName: 'Banh Xeo', NumOfProducts: 14)
// * Default Arguments
//    Example:
//        ProductManager({this.ProductName = 'Cake'})
// * Optional Arguments
//    Example:
//        ProductManager([this.NumOfProducts])
//
// ----------------------------------------
// Passing Data
// ----------------------------------------
// * Passing data between widgets by using contuctor methods.
// * For Stateful widgets, data can be passed from widget to
//   state by using the special widget property.

// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import './pages/module044_auth.dart';
import './pages/module044_product.dart';
import './pages/module044_products.dart';
import './pages/module044_products_admin.dart';

// ************************************************************
// Main
// ************************************************************
void main() {
  // Enable UI debug.
  debugPaintSizeEnabled = false;
  debugPaintPointersEnabled = false;
  runApp(MyApp());
}

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Main widget.
class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  List<Map<String, dynamic>> mProducts = [];

  void _addProduct(Map<String, dynamic> product) {
    setState(() {
      mProducts.add(product);
    });
  }

  void _deleteProduct(int index) {
    setState(() {
      mProducts.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowMaterialGrid: false,
      theme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.deepPurple),
      //home: AuthPage(),
      routes: {
        // NB! The slash name is the same as home. If we have both
        // we would get an error.
        '/': (BuildContext context) => AuthPage(),
        '/products': (BuildContext context) => ProductsPage(mProducts),
        '/admin': (BuildContext context) =>
            ProductsAdminPage(_addProduct, _deleteProduct),
      },
      // This would be generated if no name route are specified.
      onGenerateRoute: (RouteSettings settings) {
        final List<String> pathElements = settings.name.split('/');
        if (pathElements[0] != '') {
          return null;
        }
        if (pathElements[1] == 'product') {
          final int index = int.parse(pathElements[2]);

          return MaterialPageRoute<bool>(
            builder: (BuildContext context) => ProductPage(
                  mProducts[index]['title'],
                  mProducts[index]['image'],
                  mProducts[index]['price'],
                  mProducts[index]['description'],
                ),
          );
        }
        return null;
      },
      onUnknownRoute: (RouteSettings settings) {
        print('[Main] Warning! Unknown route!');
        print('[Main] Route (' + settings.name + ') doesn\'s exists.');
        return MaterialPageRoute(
            builder: (BuildContext context) => ProductsPage(mProducts));
      },
    );
  }
}
