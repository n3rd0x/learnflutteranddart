/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module045_card.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  /// @brief List of products.
  final List<Map<String, dynamic>> mProducts;

  /// @brief Constructor with value initialisation.
  Products(this.mProducts) {
    print('[ProductsStatelessWidget] (Constructor)');
  }

  Widget _buildProdictList() {
    Widget productCards;
    if (mProducts.length > 0) {
      productCards = ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              ProductCard(mProducts[index], index),
          itemCount: mProducts.length);
    } else {
      // If we want to render "nothing" we should render an empty Container.
      //productCards = Center(child: Text('No products found, please add some.'));
      productCards = Container();
    }
    return productCards;
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsStatelessWidget] (Build)');
    return _buildProdictList();
  }
}
