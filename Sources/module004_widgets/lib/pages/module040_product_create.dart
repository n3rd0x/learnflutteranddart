/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductCreatePage extends StatefulWidget {
  final Function addProduct;

  ProductCreatePage(this.addProduct);

  @override
  State<StatefulWidget> createState() {
    return _ProductCreatePageState();
  }
}

class _ProductCreatePageState extends State<ProductCreatePage> {
  // Visual indicate these are private with "_" prefix.
  String _Title = '';
  String _Description = "";
  double _Price = 0.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      // We will change from Column to ListView, so the app will render
      // the "missing" parts. Because the soft key are counts as rendered
      // pixels too.
      child: ListView(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(labelText: "Product Title"),
            onChanged: (String value) {
              setState(() {
                _Title = value;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(labelText: "Product Description"),
            maxLines: 4,
            onChanged: (String value) {
              setState(() {
                _Description = value;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(labelText: "Product Price"),
            keyboardType: TextInputType.number,
            onChanged: (String value) {
              setState(() {
                _Price = double.parse(value);
              });
            },
          ),
          SizedBox(height: 28.0),
          RaisedButton(
              child: Text('Save'),
              color: Theme.of(context).accentColor,
              onPressed: () {
                final Map<String, dynamic> product = {
                  'title': _Title,
                  'description': _Description,
                  'price': _Price,
                  'image': 'assets/food.jpeg'
                };
                widget.addProduct(product);
                Navigator.pushReplacementNamed(context, '/products');
              }),
        ],
      ),
    );
  }
}
