/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  /// @brief List of products.
  final List<Map<String, dynamic>> mProducts;

  /// @brief Constructor with value initialisation.
  Products(this.mProducts) {
    print('[ProductsStatelessWidget] (Constructor)');
  }

  /// @return Card object.
  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset(mProducts[index]['image']),
          // We can also use the Padding widget.
          Container(
            padding: EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  mProducts[index]['title'],
                  style: TextStyle(
                      fontSize: 26.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Oswald'),
                ),
                SizedBox(width: 8.0),
                // We can also use DecoratedBox, but contains less properties.
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Theme.of(context).accentColor),
                  padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.5),
                  child: Text(
                    '\$${mProducts[index]['price'].toString()}',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
          // We can also use Container, it would be better performance,
          // hence we don't have any unneccessary tree of widgets.
          DecoratedBox(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.orange, width: 1.0),
                borderRadius: BorderRadius.circular(6.0)),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.5),
                child: Text('Union Square, San Francisco')),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text('Details'),
                onPressed: () => Navigator.pushNamed<bool>(
                    context, '/product/' + index.toString()),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildProdictList() {
    Widget productCards;
    if (mProducts.length > 0) {
      productCards = ListView.builder(
          itemBuilder: _buildProductItem, itemCount: mProducts.length);
    } else {
      // If we want to render "nothing" we should render an empty Container.
      //productCards = Center(child: Text('No products found, please add some.'));
      productCards = Container();
    }
    return productCards;
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsStatelessWidget] (Build)');
    return _buildProdictList();
  }
}
