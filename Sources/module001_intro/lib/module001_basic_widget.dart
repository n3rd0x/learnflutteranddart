/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */

// ************************************************************
// Import
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Main
// ************************************************************
main() {
  runApp(MyApp());
}

// This is equivalent to above.
// NB! This only works if there are only one statement.
// main() => runApp(MyApp());

// ************************************************************
// Member Declarations
// ************************************************************
class MyApp extends StatelessWidget {
  build(context) {
    // Scaffold widget create a new page in the app.
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Basic Widgets"),
        ),
        body: Card(),
      ),
    );
  }
}
