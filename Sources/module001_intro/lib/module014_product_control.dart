/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Render a button widget
class ProductControl extends StatelessWidget {
  /// @brief Store reference to a function. Its name can be arbitary.
  final Function addProduct;
  // Alt. final addProduct;

  /// @brief Contrcutor with reference to the add function.
  ProductControl(this.addProduct);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Theme.of(context).primaryColor,
      onPressed: () {
        addProduct('Sweet');
      },
      child: Text('Add Product')
    );
  }
}