/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module007_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product manager widget
class ProductManager extends StatefulWidget {
  /// @brief The starting product.
  final String mStartProduct;

  /// @brief Contructor with value initialisation.
  ProductManager(this.mStartProduct);

  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}

/// @brief State of the ProductManager.
class _ProductManagerState extends State<ProductManager> {
  /// @brief List of products.
  List<String> mProducts = [];

  @override
  void initState() {
    mProducts.add(widget.mStartProduct);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0),
          child: RaisedButton(
            onPressed: () {
              setState(() {
                mProducts.add('Cake');
              });
            },
            child: Text('Add Product'),
          ),
        ),
        Products(mProducts)
      ],
    );
  }
}
