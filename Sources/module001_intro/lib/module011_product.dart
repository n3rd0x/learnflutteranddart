/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  /// @brief List of products.
  final List<String> mProducts;

  /// @brief Constructor with value initialisation.
  Products([this.mProducts = const []]) {
    print('[ProductsStatelessWidget] (Constructor)');
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsStatelessWidget] (Build)');
    return Column(
      children: mProducts
          .map(
            (e) => Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('assets/food.jpeg'),
                      Text(e)
                    ],
                  ),
                ),
          )
          .toList(),
    );
  }
}
