/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Render products.
class Products extends StatelessWidget {
  /// @brief List of products.
  final List<String> mProducts;

  /// @brief Constrcutor with initialisation of values.
  Products(this.mProducts);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: mProducts
          .map(
            (element) => Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('assets/food.jpeg'),
                      Text(element)
                    ],
                  ),
                ),
          )
          .toList(),
    );
  }
}
