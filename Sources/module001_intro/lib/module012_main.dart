/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Tips
// ************************************************************
// ----------------------------------------
// Generic
// ----------------------------------------
// * Underscore
//      Use when we want thing to be private.
// * Final
//      Make the target imutable, so we can't change the reference of
//      the variable. Or assign new reference or value.
//      NB! For list we can still modify its value, but not assign
//      new list.
// * Const
//      This make we can't change the value.
//
// ----------------------------------------
// Setup Assets
// ----------------------------------------
// In order to use the assets directory, we have to edit the pubspec.yaml.
// Uncomment the "asset" section and add files to use.
// This would be bundle in the app during shipment.
//
// ----------------------------------------
// App Name
// ----------------------------------------
// Android: Edit AndroidManifest.xml
// IOS:     Edit Info.plist
//
// ----------------------------------------
// Widget Types
// ----------------------------------------
// StatelessWidget
//  * Only take data as input (optional), then render a new widget tree.
//  * Useful for render static page.
// StatefulWidget
//  * Work with external and internal data which can be changed.
//  * Useful for dynamic page.
//  * Offer additional lifecycle methods.
// Changes to external or internal data lead to a rerender cycle.
//
// ----------------------------------------
// Rebuild Widgets
// ----------------------------------------
// State widgets can be rebuild when we are passing new
// values into the contructor, which will again call the build
// function.
//
// ----------------------------------------
// Keywords
// ----------------------------------------
// * widget (properties)
//    Allow access between the connected state classes.
//
// ----------------------------------------
// Arguments
// ----------------------------------------
// * Positional Arguments
//    Arguments are passed by its position declarations.
//    Example:
//        ProductManager(this.ProductName, this.NumOfProducts)
//        Usage:
//        ProductManager('Banh Xeo', 14)
// * Named Arguments
//    Arguments are passed by name.
//    Example:
//        ProductManager({this.ProductName}, {this.NumOfProducts})
//        Usage:
//        ProductManager(ProductName: 'Banh Xeo', NumOfProducts: 14)
// * Default Arguments
//    Example:
//        ProductManager({this.ProductName = 'Cake'})
// * Optional Arguments
//    Example:
//        ProductManager([this.NumOfProducts])
//
// ----------------------------------------
// Passing Data
// ----------------------------------------
// * Passing data between widgets by using contuctor methods.
// * For Stateful widgets, data can be passed from widget to
//   state by using the special widget property.

// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import './module011_product_manager.dart';


// ************************************************************
// Main
// ************************************************************
void main() {
  // Enable UI debug.
  debugPaintSizeEnabled = true;
  debugPaintPointersEnabled = true;
  runApp(MyApp());
}

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Main widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowMaterialGrid: true,
      theme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.deepPurple),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Material Design"),
        ),
        body: ProductManager(mStartProduct: 'Banh Xeo'),
      ),
    );
  }
}
