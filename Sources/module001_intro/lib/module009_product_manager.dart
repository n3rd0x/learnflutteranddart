/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module009_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product manager widget
class ProductManager extends StatefulWidget {
  /// @brief The starting product.
  final String mStartProduct;

  /// @brief Contructor with value initialisation.
  ProductManager(this.mStartProduct) {
    print('[ProductManagerStatefulWidget] (Constructor)');
  }

  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}

/// @brief State of the ProductManager.
class _ProductManagerState extends State<ProductManager> {
  /// @brief List of products.
  List<String> mProducts = [];

  @override
  void initState() {
    print('[ProductManagerStatefulWidget] (InitState)');
    mProducts.add(widget.mStartProduct);
    super.initState();
  }

  @override
  void didUpdateWidget(ProductManager oldWidget) {
    print('[ProductManagerStatefulWidget] didUpdateWidet');
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerStatefulWidget] (Build)');
    return Column(
      children: [
        Container(
          child: RaisedButton(
              color: Theme.of(context).primaryColor,
              onPressed: () {
                setState(() {
                  mProducts.add('Cake');
                });
              },
              child: Text('Add Product')),
        ),
        Products(mProducts)
      ],
    );
  }
}
