/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  /// @brief List of products.
  final List<String> mProducts;

  /// @brief Constructor with value initialisation.
  Products([this.mProducts = const []]) {
    print('[ProductsStatelessWidget] (Constructor)');
  }

  /// @return Card object.
  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset('assets/food.jpeg'),
          Text(mProducts[index])
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsStatelessWidget] (Build)');
    // Using this way of ListView when we have dynamic objects
    // would be more performance efficiency.
    return ListView.builder(
      itemBuilder: _buildProductItem,
      itemCount: mProducts.length,
    );
  }
}
