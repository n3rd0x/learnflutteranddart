/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Tips
// ************************************************************
// ----------------------------------------
// Setup Assets
// ----------------------------------------
// In order to use the assets directory, we have to edit the pubspec.yaml.
// Uncomment the "asset" section and add files to use.
// This would be bundle in the app during shipment.
//
// ----------------------------------------
// App Name
// ----------------------------------------
// Android: Edit AndroidManifest.xml
// IOS:     Edit Info.plist
//
// ----------------------------------------
// Widget Types
// ----------------------------------------
// StatelessWidget
//  * Only take data as input (optional), then render a new widget tree.
//  * Useful for render static page.
// StatefulWidget
//  * Work with external and internal data which can be changed.
//  * Offer additional lifecycle methods.
//  * Useful for dynamic page.
// Changes to external or internal data lead to a rerender cycle.
//
// ----------------------------------------
// Rebuild Widgets
// ----------------------------------------
// State widgets can be rebuild when we are passing new
// values into the contructor, which will again call the build
// function.
//
// ----------------------------------------
// Keywords
// ----------------------------------------
// * widget (properties)
//    Allow access between the connected state classes.

// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module007_product_manager.dart';

// ************************************************************
// Main
// ************************************************************
// This is equivalent to below.
// NB! This only works if there are only one statement.
// void main() {
//    runApp(MyApp());
// }
void main() => runApp(MyApp());

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Main widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Product Manager Data"),
        ),
        body: ProductManager('Banh Xeo'),
      ),
    );
  }
}
