/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Tips
// ************************************************************
// Setup Assets:
// In order to use the assets directory, we have to edit the pubspec.yaml.
// Uncomment the "asset" section and add files to use.
// This would be bundle in the app during shipment.

// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Main
// ************************************************************
void main() => runApp(MyApp());

// ************************************************************
// Member Declarations
// ************************************************************
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Card & Image")),
        body: Card(
          child: Column(
            children: <Widget>[
              Image.asset("assets/food.jpeg"),
              Text("Banh Xeo"),
            ],
          ),
        ),
      ),
    );
  }
}
