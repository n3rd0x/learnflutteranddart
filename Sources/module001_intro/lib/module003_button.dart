/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Tips
// ************************************************************
// ----------------------------------------
// Setup Assets
// ----------------------------------------
// In order to use the assets directory, we have to edit the pubspec.yaml.
// Uncomment the "asset" section and add files to use.
// This would be bundle in the app during shipment.

// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Main
// ************************************************************
// This is equivalent to below.
// NB! This only works if there are only one statement.
// void main() {
//    runApp(MyApp());
// }
void main() => runApp(MyApp());

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Main widget.
class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Button"),
        ),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10.0),
              child: RaisedButton(
                onPressed: () {},
                child: Text("Add Product"),
              ),
            ),
            Card(
              child: Column(
                children: <Widget>[
                  Image.asset("assets/food.jpeg"),
                  Text("Banh Xeo"),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
