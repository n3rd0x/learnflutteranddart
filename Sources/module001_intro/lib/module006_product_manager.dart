/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module006_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product manager
class ProductManager extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}

/// @brief State for the ProductManager.
class _ProductManagerState extends State<ProductManager> {
  List<String> mProducts = ['Banh Xeo'];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0),
          child: RaisedButton(
            onPressed: () {
              setState(() {
                mProducts.add('Cake');
              });
            },
            child: Text('Add Product'),
          ),
        ),
        Products(mProducts)
      ],
    );
  }
}
