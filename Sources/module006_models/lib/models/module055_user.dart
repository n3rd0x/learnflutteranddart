/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class User {
  final String id;
  final String email;
  final String password;

  User({@required this.id, @required this.email, @required this.password});

  void printUserInfo() {
    print('-- USER INFO --');
    print('ID:       ' + id);
    print('EMail:    ' + email);
    print('Password: ' + password);
    print('---------------');
  }
}
