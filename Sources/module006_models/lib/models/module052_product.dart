/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class Product {
  final String title;
  final String description;
  final double price;
  final String image;

  Product(
      {@required this.title,
      @required this.description,
      @required this.price,
      @required this.image});
}
