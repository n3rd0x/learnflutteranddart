/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../models/module052_product.dart';
import '../widgets/products/module052_product.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductsPage extends StatelessWidget {
  final Function deleteProduct;
  final List<Product> mProducts;

  ProductsPage(this.mProducts, this.deleteProduct);

  Widget _buildSideDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Text('Choose'),
          ),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Manage Products'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/admin');
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsPage] Starting.');
    return Scaffold(
      drawer: _buildSideDrawer(context),
      appBar: AppBar(
        title: Text("Product List"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.favorite),
            onPressed: () {},
          ),
        ],
      ),
      body: Products(mProducts, deleteProduct),
    );
  }
}
