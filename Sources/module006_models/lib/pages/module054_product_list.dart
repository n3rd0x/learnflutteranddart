/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../scoped-models/module054_products.dart';
import '../pages/module054_product_edit.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductListPage extends StatelessWidget {
  Widget _buildSubmitButton(
      BuildContext context, int index, ProductModel model) {
    return IconButton(
      icon: Icon(Icons.edit),
      onPressed: () {
        model.selectProduct(index);
        Navigator.of(context).push<bool>(
          MaterialPageRoute(
            builder: (BuildContext context) {
              return ProductEditPage();
            },
          ),
        ).then((bool value) {
          model.selectProduct(null);
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductListPage] Starting.');
    return ScopedModelDescendant<ProductModel>(
        builder: (BuildContext context, Widget child, ProductModel model) {
      return ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return Dismissible(
            key: Key(model.products[index].title),
            onDismissed: (DismissDirection direction) {
              if (direction == DismissDirection.endToStart) {
                print('Deleting product index: ' + index.toString());
                model.selectProduct(index);
                model.deleteProduct();
              }
            },
            background: Container(color: Colors.red),
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage(model.products[index].image),
                  ),
                  title: Text(model.products[index].description),
                  subtitle: Text('\$${model.products[index].price.toString()}'),
                  trailing: _buildSubmitButton(context, index, model),
                ),
                Divider(),
              ],
            ),
          );
        },
        itemCount: model.products.length,
      );
    });
  }
}
