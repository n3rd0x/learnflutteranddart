/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../scoped-models/module054_products.dart';
import '../models/module054_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ProductEditPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProductEditPageState();
  }
}

class _ProductEditPageState extends State<ProductEditPage> {
  final Map<String, dynamic> mFormData = {
    'title': null,
    'description': null,
    'price': null,
    'image': 'assets/food.jpeg'
  };
  final GlobalKey<FormState> mFormKey = GlobalKey<FormState>();

  Widget _buildTitleTextField(Product product) {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Title"),
        initialValue: product == null ? '' : product.title,
        validator: (String value) {
          if (value.isEmpty) {
            return 'Title is required!';
          }
          return null;
        },
        onSaved: (String value) {
          mFormData['title'] = value;
        });
  }

  Widget _buildDescriptionTextField(Product product) {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Description"),
        // We intentionally skip the validation here, because we
        // want to make the description optional.
        initialValue: product == null ? '' : product.description,
        onSaved: (String value) {
          mFormData['description'] = value;
        });
  }

  Widget _buildPriceTextField(Product product) {
    return TextFormField(
        decoration: InputDecoration(labelText: "Product Price"),
        initialValue: product == null ? '' : product.price.toString(),
        validator: (String value) {
          if (value.isEmpty ||
              !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value)) {
            return 'Price is required and should be a number!';
          }
          return null;
        },
        keyboardType: TextInputType.number,
        onSaved: (String value) {
          mFormData['price'] = double.parse(value);
        });
  }

  void _submitForm(Function addProduct, Function updateProduct,
      [int selectedIndex]) {
    if (!mFormKey.currentState.validate()) {
      return;
    }
    mFormKey.currentState.save();
    if (selectedIndex == null) {
      addProduct(Product(
          title: mFormData['title'],
          description: mFormData['description'],
          price: mFormData['price'],
          image: mFormData['image']));
    } else {
      updateProduct(Product(
          title: mFormData['title'],
          description: mFormData['description'],
          price: mFormData['price'],
          image: mFormData['image']));
    }
    Navigator.pushReplacementNamed(context, '/products');
  }

  Widget _buildSubmitButton() {
    return ScopedModelDescendant<ProductModel>(
      builder: (BuildContext context, Widget child, ProductModel model) {
        return RaisedButton(
          child: Text('Save'),
          textColor: Colors.white,
          onPressed: () => _submitForm(
              model.addProduct, model.updateProduct, model.selectedIndex),
        );
      },
    );
  }

  Widget _buildPageContent(double targetPadding, Product product) {
    print('[ProductEditPage] Build page content as child.');
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: Form(
          key: mFormKey,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: targetPadding / 2),
            children: <Widget>[
              _buildTitleTextField(product),
              _buildDescriptionTextField(product),
              _buildPriceTextField(product),
              SizedBox(height: 10.0),
              _buildSubmitButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPageWithContent(double targetPadding, Product product) {
    print('[ProductEditPage] Build parent Scaffold.');
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Page'),
      ),
      body: _buildPageContent(targetPadding, product),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductEditPage] Starting.');
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    final double targetPadding = deviceWidth - targetWidth;
    return ScopedModelDescendant<ProductModel>(
      builder: (BuildContext context, Widget child, ProductModel model) {
        Product curProduct = model.selectedProduct;
        return (curProduct == null)
            ? _buildPageContent(targetPadding, curProduct)
            : _buildPageWithContent(targetPadding, curProduct);
      },
    );
  }
}
