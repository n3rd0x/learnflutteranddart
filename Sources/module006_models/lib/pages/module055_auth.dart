/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../scoped-models/module055_main.dart';
import '../models/module055_user.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  String mEmail = '';
  String mPassword = '';
  bool mAcceptTerms = false;
  final GlobalKey<FormState> mFormKey = GlobalKey<FormState>();

  DecorationImage _buildBackgroundImage() {
    return DecorationImage(
      fit: BoxFit.cover,
      colorFilter:
          ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
      image: AssetImage('assets/background.jpg'),
    );
  }

  Widget _buildEmailTextField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: 'E-Mail', filled: true, fillColor: Colors.white),
        keyboardType: TextInputType.emailAddress,
        validator: (String value) {
          return null;
        },
        onSaved: (String value) {
          mEmail = value;
        });
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Password', filled: true, fillColor: Colors.white),
      obscureText: true,
      validator: (String value) {
        return null;
      },
      onSaved: (String value) {
        mPassword = value;
      },
    );
  }

  Widget _buildAcceptSwitch() {
    return SwitchListTile(
      value: mAcceptTerms,
      onChanged: (bool value) {
        setState(() {
          mAcceptTerms = value;
        });
      },
      title: Text('Accept Terms'),
    );
  }

  void _submitForm(MainModel model) {
    if (!mFormKey.currentState.validate()) {
      return;
    }
    mFormKey.currentState.save();
    if ((mEmail != '') || (mPassword != '')) {
      print('Login with credentials:');
    }
    if (mEmail != '') {
      print('EMail: ' + mEmail);
    }
    if (mPassword != '') {
      print('Password: ' + mPassword);
    }
    model.login(mEmail, mPassword);
    model.printUserInfo();
    Navigator.pushReplacementNamed(context, '/products');
  }

  @override
  Widget build(BuildContext context) {
    print('[Auth] Starting');
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    return Scaffold(
      appBar: AppBar(title: Text('Add User Model')),
      body: Container(
        decoration: BoxDecoration(
          image: _buildBackgroundImage(),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              width: targetWidth,
              child: Form(
                key: mFormKey,
                child: ScopedModelDescendant<MainModel>(
                  builder:
                      (BuildContext context, Widget child, MainModel model) {
                    return Column(
                      children: <Widget>[
                        _buildEmailTextField(),
                        SizedBox(height: 10.0),
                        _buildPasswordTextField(),
                        _buildAcceptSwitch(),
                        SizedBox(height: 10.0),
                        RaisedButton(
                          textColor: Colors.white,
                          child: Text('LOGIN'),
                          onPressed: () {
                            _submitForm(model);
                          },
                        ),
                        GestureDetector(
                          onDoubleTap: () {
                            _submitForm(model);
                          },
                          child: Container(
                            color: Colors.green,
                            padding: EdgeInsets.all(5.0),
                            child:
                                Text('You can also double tap here to log in.'),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
