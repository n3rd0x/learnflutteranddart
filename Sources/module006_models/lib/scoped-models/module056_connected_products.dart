/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:scoped_model/scoped_model.dart';
import '../models/module056_product.dart';
import '../models/module056_user.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ConnectedProductsModel extends Model {
  List<Product> _products = [];
  int _selectedIndex;
  User _authUser = null;

  void addProduct(
      String title, String description, String image, double price) {
    final Product newProduct = Product(
        title: title,
        description: description,
        image: image,
        price: price,
        userEmail: _authUser.email,
        userId: _authUser.id);

    _products.add(newProduct);
    notifyListeners();
  }
}

class ProductModel extends ConnectedProductsModel {
  bool _showFavorites = false;

  List<Product> get displayProducts {
    if (_showFavorites) {
      // Alternative:
      // List.from(_products.where((Product product) => product.isFavorite).toList());
      return List.from(_products.where((Product product) {
        return product.isFavorite;
      }).toList());
    }
    return List.from(_products);
  }

  List<Product> get allProducts {
    return List.from(_products);
  }

  void deleteProduct() {
    _products.removeAt(_selectedIndex);
    notifyListeners();
  }

  bool get isShowFavorites {
    return _showFavorites;
  }

  int get selectedIndex {
    return _selectedIndex;
  }

  Product get selectedProduct {
    if (_selectedIndex != null) {
      return _products[_selectedIndex];
    }
    return null;
  }

  void selectProduct(int index) {
    _selectedIndex = index;
  }

  void toggleDisplayFavorite() {
    _showFavorites = !_showFavorites;
    notifyListeners();
  }

  void toggleFavorite() {
    final bool curState = _products[_selectedIndex].isFavorite;
    final Product updateProduct = Product(
        title: selectedProduct.title,
        description: selectedProduct.description,
        price: selectedProduct.price,
        image: selectedProduct.image,
        userEmail: selectedProduct.userEmail,
        userId: selectedProduct.userId,
        isFavorite: !curState);
    _products[_selectedIndex] = updateProduct;

    // Notify that the data have changed so the widget would be rebuild
    // with the new data. This is useful for widget that we want live updates.
    // Otherwise the update would be applied when we revisit the widget.
    notifyListeners();
  }

  void updateProduct(
      String title, String description, String image, double price) {
    final Product updatedProduct = Product(
        title: title,
        description: description,
        image: image,
        price: price,
        userEmail: selectedProduct.userEmail,
        userId: selectedProduct.userId);
    _products[_selectedIndex] = updatedProduct;
    notifyListeners();
  }
}

class UserModel extends ConnectedProductsModel {
  void login(String email, String password) {
    _authUser = User(id: "RandomId", email: email, password: password);
  }

  void printUserInfo() {
    if (_authUser != null) {
      _authUser.printUserInfo();
    } else {
      print('WARNING! NO CURRENT USER ACTIVATED!');
    }
  }
}
