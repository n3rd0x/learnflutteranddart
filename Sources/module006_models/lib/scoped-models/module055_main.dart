/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:scoped_model/scoped_model.dart';
import './module055_products.dart';
import './module055_user.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class MainModel extends Model with ProductModel, UserModel {}
