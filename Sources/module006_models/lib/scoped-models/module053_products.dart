/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:scoped_model/scoped_model.dart';
import '../models/module053_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ProductModel extends Model {
  List<Product> mProducts = [];
  int mSelectedIndex;

  List<Product> get products {
    return List.from(mProducts);
  }

  void addProduct(Product product) {
    mProducts.add(product);
    mSelectedIndex = null;
  }

  void deleteProduct() {
    if (mSelectedIndex != null) {
      mProducts.removeAt(mSelectedIndex);
      mSelectedIndex = null;
    }
  }

  int selectedIndex() {
    return mSelectedIndex;
  }

  Product selectedProduct() {
    if (mSelectedIndex != null) {
      return mProducts[mSelectedIndex];
    }
    return null;
  }

  void selectProduct(int index) {
    mSelectedIndex = index;
  }

  void updateProduct(Product product) {
    if (mSelectedIndex != null) {
      mProducts[mSelectedIndex] = product;
      mSelectedIndex = null;
    }
  }
}
