/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:scoped_model/scoped_model.dart';
import '../models/module055_user.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class UserModel extends Model {
  User mAuthUser = null;

  void login(String email, String password) {
    mAuthUser = User(id: "RandomId", email: email, password: password);
  }

  void printUserInfo() {
    if (mAuthUser != null) {
      mAuthUser.printUserInfo();
    } else {
      print('WARNING! NO CURRENT USER ACTIVATED!');
    }
  }
}
