/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:scoped_model/scoped_model.dart';
import '../models/module055_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ProductModel extends Model {
  List<Product> mProducts = [];
  int mSelectedIndex;
  bool mShowFavorites = false;

  List<Product> get displayProducts {
    if (mShowFavorites) {
      // Alternative:
      // List.from(mProducts.where((Product product) => product.isFavorite).toList());
      return List.from(mProducts.where((Product product) {
        return product.isFavorite;
      }).toList());
    }
    return List.from(mProducts);
  }

  List<Product> get products {
    return List.from(mProducts);
  }

  void addProduct(Product product) {
    mProducts.add(product);
    mSelectedIndex = null;
  }

  void deleteProduct() {
    if (mSelectedIndex != null) {
      mProducts.removeAt(mSelectedIndex);
      mSelectedIndex = null;
    }
  }

  bool get isShowFavorites {
    return mShowFavorites;
  }

  int get selectedIndex {
    return mSelectedIndex;
  }

  Product get selectedProduct {
    if (mSelectedIndex != null) {
      return mProducts[mSelectedIndex];
    }
    return null;
  }

  void selectProduct(int index) {
    mSelectedIndex = index;
  }

  void toggleDisplayFavorite() {
    mShowFavorites = !mShowFavorites;
    notifyListeners();
  }

  void toggleFavorite() {
    final bool curState = mProducts[mSelectedIndex].isFavorite;
    final Product nproduct = Product(
        title: selectedProduct.title,
        description: selectedProduct.description,
        price: selectedProduct.price,
        image: selectedProduct.image,
        isFavorite: !curState);
    updateProduct(nproduct);

    // Notify that the data have changed so the widget would be rebuild
    // with the new data. This is useful for widget that we want live updates.
    // Otherwise the update would be applied when we revisit the widget.
    notifyListeners();
  }

  void updateProduct(Product product) {
    if (mSelectedIndex != null) {
      mProducts[mSelectedIndex] = product;
      mSelectedIndex = null;
    }
  }
}
