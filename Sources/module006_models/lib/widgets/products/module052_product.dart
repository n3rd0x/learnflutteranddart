/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../../models/module052_product.dart';
import './module052_card.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  final Function deleteProduct;

  /// @brief List of products.
  final List<Product> mProducts;

  /// @brief Constructor with value initialisation.
  Products(this.mProducts, this.deleteProduct);

  Widget _buildProdictList() {
    print('[Products] Create list of products....');
    Widget productCards;
    if (mProducts.length > 0) {
      print('[Products] Number to create: ' + mProducts.length.toString());
      productCards = ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              ProductCard(mProducts[index], index, deleteProduct),
          itemCount: mProducts.length);
    } else {
      // If we want to render "nothing" we should render an empty Container.
      //productCards = Center(child: Text('No products found, please add some.'));
      print('[Products] No products exists.');
      productCards = Container();
    }
    return productCards;
  }

  @override
  Widget build(BuildContext context) {
    print('[Products] Starting.');
    return _buildProdictList();
  }
}
