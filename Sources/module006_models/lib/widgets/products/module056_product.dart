/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped-models/module056_main.dart';
import '../../models/module056_product.dart';
import './module056_card.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  Widget _buildProdictList(List<Product> products) {
    print('[Products] Create list of products....');
    Widget productCards;
    if (products.length > 0) {
      print('[Products] Number to create: ' + products.length.toString());
      productCards = ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              ProductCard(products[index], index),
          itemCount: products.length);
    } else {
      // If we want to render "nothing" we should render an empty Container.
      //productCards = Center(child: Text('No products found, please add some.'));
      print('[Products] No products exists.');
      productCards = Container();
    }
    return productCards;
  }

  @override
  Widget build(BuildContext context) {
    print('[Products] Starting.');
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        // TST 2019-08-25
        // There is a bug when we are filtering on the favorites.
        // Because the index of the favorites list, may not refer to
        // the same index of the products list.
        return _buildProdictList(model.displayProducts);
      },
    );
  }
}
