/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../../models/module052_product.dart';
import '../products/module052_address_tag.dart';
import '../products/module052_price_tag.dart';
import '../ui/module052_title_default.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product card
class ProductCard extends StatelessWidget {
  final Function deleteProduct;
  final Product mProduct;
  final int mProductIndex;

  ProductCard(this.mProduct, this.mProductIndex, this.deleteProduct);

  Widget _buildTitlePriceRow() {
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TitleDefault(mProduct.title),
          SizedBox(width: 8.0),
          PriceTag(mProduct.price.toString()),
        ],
      ),
    );
  }

  Widget _buildActionButtons(BuildContext context) {
    return ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.info),
          color: Theme.of(context).accentColor,
          onPressed: () => Navigator.pushNamed<bool>(
                  context, '/product/' + mProductIndex.toString() + '/editable')
              .then((bool value) {
            if (value) {
              deleteProduct(mProductIndex);
            }
          }),
        ),
        IconButton(
          icon: Icon(Icons.favorite_border),
          color: Colors.red,
          onPressed: () => Navigator.pushNamed<bool>(
              context, '/product/' + mProductIndex.toString()),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductCard] (' +
        mProduct.title +
        ', ' +
        mProductIndex.toString() +
        ').');
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset(mProduct.image),
          _buildTitlePriceRow(),
          AddressTag('Union Square, San Francisco'),
          _buildActionButtons(context),
        ],
      ),
    );
  }
}
