/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'dart:async';
import 'package:flutter/material.dart';
import '../models/product.dart';
import '../widgets/ui/title_default.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductPage extends StatelessWidget {
  final Product mProduct;
  final bool readOnly;

  ProductPage(this.mProduct, {this.readOnly = true});

  Widget _buildAction(BuildContext context) {
    if (!readOnly) {
      return Container(
        padding: EdgeInsets.all(10.0),
        child: RaisedButton(
          color: Theme.of(context).accentColor,
          child: Text("DELETE"),
          onPressed: () => _showWarningDialog(context),
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.all(10.0),
        child: RaisedButton(
          color: Theme.of(context).accentColor,
          child: Text("BACK"),
          onPressed: () => Navigator.pop(context),
        ),
      );
    }
  }

  Widget _buildAddressPriceRow(double price) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Union Square, San Francisco',
          style: TextStyle(fontFamily: 'Oswald', color: Colors.grey),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 5.0),
          child: Text(
            '|',
            style: TextStyle(color: Colors.grey),
          ),
        ),
        Text(
          '\$' + price.toString(),
          style: TextStyle(fontFamily: 'Oswald', color: Colors.grey),
        ),
      ],
    );
  }

  void _showWarningDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('Are you sure?'),
              content: Text('This action cannot be undone!'),
              actions: <Widget>[
                FlatButton(
                  child: Text('DISCARD'),
                  textTheme: ButtonTheme.of(context).textTheme,
                  onPressed: () {
                    // This close the dialog, not the page.
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  child: Text('CONTINUE'),
                  textTheme: ButtonTheme.of(context).textTheme,
                  color: Colors.redAccent,
                  onPressed: () {
                    // This close the dialog.
                    Navigator.pop(context);

                    // This close and flag to delete the content as before.
                    Navigator.pop(context, true);
                  },
                ),
              ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    String editable = 'READ_ONLY';
    if (!readOnly) {
      editable = 'EDITABLE';
    }
    print('[Product] (' + mProduct.title + ', ' + editable + ').');

    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);

        // If we use TRUE, it would add another pop,
        // since we are at root, it would crash the app.
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(mProduct.title),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FadeInImage(
                image: NetworkImage(mProduct.image),
                height: 300.0,
                fit: BoxFit.cover,
                placeholder: AssetImage('assets/food.jpeg')),
            // Image.asset => local file.
            // Image.network(product.image),
            Container(
              padding: EdgeInsets.all(10.0),
              child: TitleDefault(mProduct.description),
            ),
            _buildAddressPriceRow(mProduct.price),
            _buildAction(context),
          ],
        ),
      ),
    );
  }
}
