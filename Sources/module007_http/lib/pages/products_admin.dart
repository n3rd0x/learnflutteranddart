/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../scoped-models/main.dart';
import '../pages/product_edit.dart';
import '../pages/product_list.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductsAdminPage extends StatelessWidget {
  final MainModel model;

  ProductsAdminPage(this.model);

  Widget _buildSideDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Text('Choose'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/');
                },
              ),
            ],
          ),
          ListTile(
            leading: Icon(Icons.shop),
            title: Text('All Products'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/products');
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsAdmin] Starting.');
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: _buildSideDrawer(context),
        appBar: AppBar(
          title: Text('Manage Products'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.create),
                text: 'Create Product',
              ),
              Tab(
                icon: Icon(Icons.widgets),
                text: 'My Products',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            // NB! Here the widgets would be rendered as sub-page,
            // so we don't need to create a new Scaffold, Drawer, etc.
            // We only need to implement the body in these pages.
            ProductEditPage(),
            ProductListPage(model),
          ],
        ),
      ),
    );
  }
}
