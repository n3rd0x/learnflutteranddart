/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped-models/main.dart';
import '../../models/product.dart';
import '../products/address_tag.dart';
import '../products/price_tag.dart';
import '../ui/title_default.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product card
class ProductCard extends StatelessWidget {
  final Product mProduct;
  final int mProductIndex;

  ProductCard(this.mProduct, this.mProductIndex);

  Widget _buildTitlePriceRow() {
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TitleDefault(mProduct.title),
          SizedBox(width: 8.0),
          PriceTag(mProduct.price.toString()),
        ],
      ),
    );
  }

  Widget _buildActionButtons(BuildContext context, MainModel model) {
    return ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.info),
          color: Theme.of(context).accentColor,
          onPressed: () => Navigator.pushNamed<bool>(
                  context,
                  '/product/' +
                      model.allProducts[mProductIndex].id +
                      '/editable')
              .then((bool value) {
            if (value) {
              model.selectProduct(model.allProducts[mProductIndex].id);
              model.deleteProduct();
              model.selectProduct(null);
            }
          }),
        ),
        IconButton(
          icon: Icon(model.allProducts[mProductIndex].isFavorite
              ? Icons.favorite
              : Icons.favorite_border),
          color: Colors.red,
          onPressed: () {
            model.selectProduct(model.allProducts[mProductIndex].id);
            model.toggleFavorite();
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductCard] (' +
        mProduct.title +
        ', ' +
        mProductIndex.toString() +
        ').');
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Card(
          child: Column(
            children: <Widget>[
              FadeInImage(
                image: NetworkImage(mProduct.image),
                height: 300.0,
                fit: BoxFit.cover,
                placeholder: AssetImage('assets/food.jpeg'),
              ),
              _buildTitlePriceRow(),
              AddressTag('Union Square, San Francisco'),
              _buildActionButtons(context, model),
            ],
          ),
        );
      },
    );
  }
}
