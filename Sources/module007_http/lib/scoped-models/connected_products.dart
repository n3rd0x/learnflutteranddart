/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'dart:async';
import 'dart:convert';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import '../models/product.dart';
import '../models/user.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class ConnectedProductsModel extends Model {
  final String _url = 'https://education-flutter.firebaseio.com/products';
  final String _imageUrl =
      'https://cdn-prod.medicalnewstoday.com/content/images/articles/317/317122/junk-food.jpg';
  List<Product> _products = [];
  String _selectedId;
  User _authUser = null;
  bool _isLoading = false;
  bool _isRefreshing = false;
}

class ProductModel extends ConnectedProductsModel {
  bool _showFavorites = false;

  Future<String> addProduct(
      String title, String description, String image, double price) async {
    _isLoading = true;
    notifyListeners();

    // Create a post to the Firebase.
    final Map<String, dynamic> productData = {
      'title': title,
      'description': description,
      'image': _imageUrl,
      'price': price,
      'userEmail': _authUser.email,
      'userId': _authUser.id
    };

    print('[ConnectedProduct] Attempt adding product(' + title + ').');

    try {
      // This would automaticcally convert into the same code as
      // previous when using "then".
      final http.Response response =
          await http.post(_url + ".json", body: json.encode(productData));

      // Check error status.
      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;

        print(
            '[ConnectedProduct] StatusCode: ' + response.statusCode.toString());
        return 'Status code: ' + response.statusCode.toString();
      }

      final Map<String, dynamic> responseData = json.decode(response.body);
      print('[ConnectedProduct] addProduct: ' + responseData.toString());
      final Product newProduct = Product(
          id: responseData['name'],
          title: title,
          description: description,
          image: image,
          price: price,
          userEmail: _authUser.email,
          userId: _authUser.id);

      _products.add(newProduct);

      _isLoading = false;
      notifyListeners();

      print('[ConnectedProduct] Successful adding the product.');
      return null;
    } catch (error) {
      _isLoading = false;
      notifyListeners();

      print('[ConnectedProduct] Exception: ' + error.toString());
      return error.toString();
    }
  }

  List<Product> get allProducts {
    return List.from(_products);
  }

  List<Product> get displayProducts {
    if (_showFavorites) {
      // Alternative:
      // List.from(_products.where((Product product) => product.isFavorite).toList());
      return List.from(_products.where((Product product) {
        return product.isFavorite;
      }).toList());
    }
    return List.from(_products);
  }

  void deleteProduct() {
    final deletedId = selectedProduct.id;
    _isLoading = true;
    _products.removeAt(selectedIndex);
    _selectedId = null;
    notifyListeners();

    http.delete(_url + "/${deletedId}.json").then((http.Response response) {
      _isLoading = false;
      notifyListeners();
    });
  }

  Future<Null> fetchProducts() {
    _isLoading = true;
    notifyListeners();

    return http.get(_url + ".json").then<Null>((http.Response response) {
      print('[ConnectedProduct] fetchProducts Response');

      // Data structure: Map<String, Map<String, dynamic>>
      Map<String, dynamic> listData = json.decode(response.body);
      if (listData == null) {
        _isLoading = false;
        notifyListeners();
        return;
      }

      final List<Product> products = [];
      // Iterate the list.
      listData.forEach((String id, dynamic itr) {
        print('[ConnectedProduct] Process ID: ' + id);
        products.add(Product(
            id: id,
            title: itr['title'],
            description: itr['description'],
            image: itr['image'],
            price: itr['price'], // The data is already a double.
            userEmail: itr['userEMail'],
            userId: itr['userId']));
      });
      _products = products;
      _isLoading = false;
      notifyListeners();
    });
  }

  bool get isShowFavorites {
    return _showFavorites;
  }

  Future<Null> refreshProducts() {
    _isRefreshing = true;
    return fetchProducts().then((_) {
      _isRefreshing = false;
    });
  }

  Product product(String id) {
    return allProducts.firstWhere((Product product) {
      return product.id == id;
    });
  }

  String get selectedId {
    return _selectedId;
  }

  int get selectedIndex {
    return _products.indexWhere((Product product) {
      return product.id == _selectedId;
    });
  }

  Product get selectedProduct {
    if (_selectedId != null) {
      return _products.firstWhere((Product product) {
        return product.id == _selectedId;
      });
    }
    return null;
  }

  void selectProduct(String id) {
    _selectedId = id;
  }

  void toggleDisplayFavorite() {
    _showFavorites = !_showFavorites;
    notifyListeners();
  }

  void toggleFavorite() {
    final bool curState = _products[selectedIndex].isFavorite;
    final Product updateProduct = Product(
        id: selectedProduct.id,
        title: selectedProduct.title,
        description: selectedProduct.description,
        price: selectedProduct.price,
        image: selectedProduct.image,
        userEmail: selectedProduct.userEmail,
        userId: selectedProduct.userId,
        isFavorite: !curState);
    _products[selectedIndex] = updateProduct;

    // Notify that the data have changed so the widget would be rebuild
    // with the new data. This is useful for widget that we want live updates.
    // Otherwise the update would be applied when we revisit the widget.
    notifyListeners();
  }

  Future<bool> updateProduct(
      String title, String description, String image, double price) {
    _isLoading = true;
    notifyListeners();

    // Update data to database.
    final Map<String, dynamic> data = {
      'title': title,
      'description': description,
      'image': _imageUrl,
      'price': price,
      'userEmail': selectedProduct.userEmail,
      'userId': selectedProduct.userId
    };
    return http
        .put(_url + "/${selectedProduct.id}.json", body: json.encode(data))
        .then<bool>((http.Response response) {
      // Check error status.
      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;
        notifyListeners();
        return false;
      }

      _products[selectedIndex] = Product(
          id: selectedProduct.id,
          title: title,
          description: description,
          image: image,
          price: price,
          userEmail: selectedProduct.userEmail,
          userId: selectedProduct.userId);

      _isLoading = false;
      notifyListeners();
      return true;
    }).catchError((onError) {
      _isLoading = false;
      notifyListeners();
      return false;
    });
  }
}

class UserModel extends ConnectedProductsModel {
  void login(String email, String password) {
    _authUser = User(id: "RandomId", email: email, password: password);
  }

  void printUserInfo() {
    if (_authUser != null) {
      _authUser.printUserInfo();
    } else {
      print('WARNING! NO CURRENT USER ACTIVATED!');
    }
  }
}

class UtilityModel extends ConnectedProductsModel {
  bool get isLoading {
    return _isLoading;
  }

  bool get isRefreshing {
    return _isRefreshing;
  }
}
