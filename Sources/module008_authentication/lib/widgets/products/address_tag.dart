/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Address tag
class AddressTag extends StatelessWidget {
  final String mTitle;

  AddressTag(this.mTitle);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.5),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.orange, width: 1.0),
          borderRadius: BorderRadius.circular(6.0)),
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.5),
          child: Text(mTitle)),
    );
  }
}
