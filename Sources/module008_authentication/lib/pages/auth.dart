/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../scoped-models/main.dart';
import '../models/auth.dart';

// ************************************************************
// Member Declarations
// ************************************************************
class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  String mEmail = '';
  String mPassword = '';
  bool mAcceptTerms = false;
  String mDatabase = "";
  String mApiKey = "";
  AuthMode mAuthMode = AuthMode.Login;
  final GlobalKey<FormState> mFormKey = GlobalKey<FormState>();
  final TextEditingController mEmailCtrl = TextEditingController();
  final TextEditingController mPasswordCtl = TextEditingController();
  final TextEditingController mDatabaseCtrl = TextEditingController();
  final TextEditingController mApiKeyCtrl = TextEditingController();

  @override
  void initState() {
    print('[AuthPage Init state.]');
    _read();
    super.initState();
  }

  DecorationImage _buildBackgroundImage() {
    return DecorationImage(
      fit: BoxFit.cover,
      colorFilter:
          ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
      image: AssetImage('assets/background.jpg'),
    );
  }

  Widget _buildEmailTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'E-Mail', filled: true, fillColor: Colors.white),
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        return null;
      },
      onSaved: (String value) {
        mEmail = value;
      },
      controller: mEmailCtrl,
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Password', filled: true, fillColor: Colors.white),
      obscureText: true,
      validator: (String value) {
        return null;
      },
      onSaved: (String value) {
        mPassword = value;
      },
      controller: mPasswordCtl,
    );
  }

  Widget _buildPasswordConfirmTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Confirm Password', filled: true, fillColor: Colors.white),
      obscureText: true,
      validator: (String value) {
        if (mPasswordCtl.text != value) {
          return 'The password doesn\'t match!';
        }
        return null;
      },
      controller: mPasswordCtl,
    );
  }

  Widget _buildServerField() {
    return Column(children: <Widget>[
      TextFormField(
        decoration: InputDecoration(
            labelText: 'Database', filled: true, fillColor: Colors.white),
        validator: (String value) {
          return null;
        },
        onSaved: (String value) {
          mDatabase = value;
        },
        controller: mDatabaseCtrl,
      ),
      SizedBox(height: 10.0),
      TextFormField(
        decoration: InputDecoration(
            labelText: 'API Key', filled: true, fillColor: Colors.white),
        validator: (String value) {
          return null;
        },
        onSaved: (String value) {
          mApiKey = value;
        },
        controller: mApiKeyCtrl,
      ),
    ]);
  }

  Widget _buildAcceptSwitch() {
    return SwitchListTile(
      value: mAcceptTerms,
      onChanged: (bool value) {
        setState(() {
          mAcceptTerms = value;
        });
      },
      title: Text('Accept Terms'),
    );
  }

  void _read() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      mEmailCtrl.text = prefs.getString('UserEmail');
      mPasswordCtl.text = prefs.getString('UserPassword');
      mDatabaseCtrl.text = prefs.getString('Database');
      mApiKeyCtrl.text = prefs.getString('ApiKey');

      print('[AuthPage] Read credential preferences.');
      print('[AuthPage] Email:    ' + mEmailCtrl.text);
      print('[AuthPage] Password: ' + mPasswordCtl.text);
      print('[AuthPage] Database: ' + mDatabaseCtrl.text);
      print('[AuthPage] ApiKey:   ' + mApiKeyCtrl.text);
    } catch (error) {
      print('[AuthPage] Read exception: ' + error.toString());
    }
  }

  void _save() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('Database', mDatabase);
      prefs.setString('ApiKey', mApiKey);

      print('[AuthPage] Save credential preferences.');
      print('[AuthPage] Database: ' + mDatabaseCtrl.text);
      print('[AuthPage] ApiKey:   ' + mApiKeyCtrl.text);
    } catch (error) {
      print('[AuthPage] Save exception: ' + error.toString());
    }
  }

  void _submitForm(MainModel model) async {
    if (!mFormKey.currentState.validate()) {
      return;
    }
    mFormKey.currentState.save();
    model.setServer(mDatabase, mApiKey);
    final res = await model.authenticate(mEmail, mPassword, mAuthMode);
    if (res['success']) {
      //Navigator.pushReplacementNamed(context, '/');
      _save();
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Authentication Error'),
            content: Text(res['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay!'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    print('[AuthPage] Building.');
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    return Scaffold(
      appBar: AppBar(title: Text('Module Authentication')),
      body: Container(
        decoration: BoxDecoration(
          image: _buildBackgroundImage(),
        ),
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              width: targetWidth,
              child: Form(
                key: mFormKey,
                child: ScopedModelDescendant<MainModel>(
                  builder:
                      (BuildContext context, Widget child, MainModel model) {
                    return Column(
                      children: <Widget>[
                        _buildEmailTextField(),
                        SizedBox(height: 10.0),
                        _buildPasswordTextField(),
                        SizedBox(height: 10.0),
                        mAuthMode == AuthMode.SignUp
                            ? _buildPasswordConfirmTextField()
                            : Container(),
                        _buildAcceptSwitch(),
                        SizedBox(height: 10.0),
                        _buildServerField(),
                        SizedBox(height: 10.0),
                        FlatButton(
                          child: Text(
                              'Switch to ${mAuthMode == AuthMode.Login ? 'Sign Up' : 'Login'}'),
                          onPressed: () {
                            setState(() {
                              mAuthMode = mAuthMode == AuthMode.Login
                                  ? AuthMode.SignUp
                                  : AuthMode.Login;
                            });
                          },
                        ),
                        SizedBox(height: 10.0),
                        model.isLoading
                            ? CircularProgressIndicator()
                            : RaisedButton(
                                textColor: Colors.white,
                                child: Text(
                                    '${mAuthMode == AuthMode.Login ? 'Login' : 'Sign Up'}'),
                                onPressed: () {
                                  _submitForm(model);
                                },
                              ),
                        GestureDetector(
                          onDoubleTap: () {
                            _submitForm(model);
                          },
                          child: Container(
                            color: Colors.green,
                            padding: EdgeInsets.all(5.0),
                            child:
                                Text('You can also double tap here to log in.'),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
