/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module032_product.dart';
import './module032_product_control.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product manager widget
class ProductManager extends StatelessWidget {
  final List<Map<String, String>> mProducts;
  final Function mpAddProduct;
  final Function mpDeleteProduct;

  ProductManager(this.mProducts, this.mpAddProduct, this.mpDeleteProduct);

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerStatefulWidget] (Build)');
    return Column(
      children: [
        Container(
          child: ProductControl(mpAddProduct),
        ),
        // In order for the ListView to render, we have to wrapping
        // our list into a Container or widget.
        //Container(height: 400.0, child: Products(mProducts))
        //
        // If we want to expand the remaining space after the first
        // container (that holds the button), we can use Expanded widget.
        Expanded(child: Products(mProducts, deleteProduct: mpDeleteProduct))
      ],
    );
  }
}
