/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import './module020_product.dart';
import './module020_product_control.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product manager widget
class ProductManager extends StatefulWidget {
  /// @brief The starting product.
  final String mStartProduct;

  /// @brief Contructor with value initialisation.
  ProductManager({this.mStartProduct}) {
    print('[ProductManagerStatefulWidget] (Constructor)');
  }

  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}

/// @brief State of the ProductManager.
class _ProductManagerState extends State<ProductManager> {
  /// @brief List of products.
  List<String> mProducts = [];

  void _addProduct(String product) {
    setState(() {
      mProducts.add(product);
    });
  }

  @override
  void initState() {
    print('[ProductManagerStatefulWidget] (InitState)');
    if (widget.mStartProduct != null) {
      mProducts.add(widget.mStartProduct);
    }
    super.initState();
  }

  @override
  void didUpdateWidget(ProductManager oldWidget) {
    print('[ProductManagerStatefulWidget] didUpdateWidet');
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerStatefulWidget] (Build)');
    return Column(
      children: [
        Container(
          child: ProductControl(_addProduct),
        ),
        // In order for the ListView to render, we have to wrapping
        // our list into a Container or widget.
        //Container(height: 400.0, child: Products(mProducts))
        //
        // If we want to expand the remaining space after the first
        // container (that holds the button), we can use Expanded widget.
        Expanded(child: Products(mProducts))
      ],
    );
  }
}
