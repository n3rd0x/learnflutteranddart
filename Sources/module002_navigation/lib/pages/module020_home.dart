/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../module020_product_manager.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: ProductManager(),
    );
  }
}
