/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import '../module030_product_manager.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductsPage extends StatelessWidget {
  final List<Map<String, String>> mProducts;
  final Function mpAddProduct;
  final Function mpDeleteProduct;

  ProductsPage(this.mProducts, this.mpAddProduct, this.mpDeleteProduct);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            AppBar(
              automaticallyImplyLeading: false,
              title: Text('Choose'),
            ),
            ListTile(
              title: Text('Manage Products'),
              onTap: () {
                // Intentionally make invalid name route.
                // It should be /admin
                Navigator.pushReplacementNamed(context, '/admins');
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Product List"),
      ),
      body: ProductManager(mProducts, mpAddProduct, mpDeleteProduct),
    );
  }
}
