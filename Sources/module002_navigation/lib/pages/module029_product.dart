/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'dart:async';
import 'package:flutter/material.dart';

// ************************************************************
// Member Declarations
// *********************************************************
class ProductPage extends StatelessWidget {
  final String mTitle;
  final String mImageUrl;

  ProductPage(this.mTitle, this.mImageUrl);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print("** BACK button pressed **");
        Navigator.pop(context, false);

        // If we use TRUE, it would add another pop,
        // since we are at root, it would crash the app.
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(this.mTitle),
        ),
        // Column expands its width as much as needed. If we want to
        // expand to screen width, we should wrapped the Column inside
        // a Center.
        // NB! Image expands the full width of the screen, so we can
        // skip the Center widget.
        //body: Center(
        //  child: Column(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(this.mImageUrl),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(this.mTitle),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: RaisedButton(
                  color: Theme.of(context).accentColor,
                  child: Text("DELETE"),
                  onPressed: () => Navigator.pop(context, true)),
            ),
          ],
        ),
        //),
      ),
    );
  }
}
