/**
 * Learn Flutter and Dart
 * Ref: https://www.packtpub.com/application-development/learn-flutter-and-dart-build-ios-and-android-apps-video
 */
// ************************************************************
// Imports
// ************************************************************
import 'package:flutter/material.dart';
import 'pages/module028_product.dart';

// ************************************************************
// Member Declarations
// ************************************************************
/// @brief Product widget
class Products extends StatelessWidget {
  /// @brief List of products.
  final List<Map<String, String>> mProducts;

  final Function deleteProduct;

  /// @brief Constructor with value initialisation.
  Products(this.mProducts, {this.deleteProduct}) {
    print('[ProductsStatelessWidget] (Constructor)');
  }

  /// @return Card object.
  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset(mProducts[index]['image']),
          Text(mProducts[index]['title']),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text('Details'),
                onPressed: () => Navigator.push<bool>(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => ProductPage(mProducts[index]['title'], mProducts[index]['image'])),
                    ).then((bool value){
                      if(value) {
                        deleteProduct(index);
                      }
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildProdictList() {
    Widget productCards;
    if (mProducts.length > 0) {
      productCards = ListView.builder(
          itemBuilder: _buildProductItem, itemCount: mProducts.length);
    } else {
      // If we want to render "nothing" we should render an empty Container.
      //productCards = Center(child: Text('No products found, please add some.'));
      productCards = Container();
    }
    return productCards;
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductsStatelessWidget] (Build)');
    return _buildProdictList();
  }
}
