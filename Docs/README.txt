====================
= Flutter
====================
# Create an app.
    - flutter create <AppName>

# Create an app with Java.
    - flutter create -a java <AppName>

# Create an app with AndroidX.
    - flutter create --androidx <AppName>

# Run an app.
    - change directory to the <AppName>
    - flutter run

# To run the app on device or emulators.
# For device:
    - flutter device


# For Android emulator:
    # List emulators.
    - flutter emulators

    # Start an emulator.
    - flutter emulators --launch <EmulatorName>

    # Use flutter run in <AppName>


# For iOS emulator:
    # Open an emulator.
    - open -a Simulator

    # Use flutter run in <AppName>


# Visual Code:
    - Open Visual Code in the <AppName> directory.
    - Open device or emulator using the "flutter emulator" command.
    - Right-click and "Start without debugging".


====================
= Install Package
====================
    - flutter packages get


====================
= Source
====================
# Source of Flutter are located in the "lib" directory.


====================
= Assets
====================
# Assets could be defined in the file "pubspec.yaml"


====================
= Setup
====================
# Visual Studio Code
    - Install extensions
        - Flutter
        - Material Icon


====================
= Tips
====================
# Visual Studio Code
    - If something is missing or incorrect, ex. missing color.
      Reinstall Flutter & Dart extensions.
    - Enable "Format on Save, Paste, Type".